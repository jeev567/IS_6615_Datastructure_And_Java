package edu.assignment5;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         15. Create a BabysittingJob class for Georgette�s Babysitting
 *         Service. The class contains fields to hold the following:
 * 
 *         * A job number that contains six digits. The first two digits
 *         represent the year, and the last four digits represent a sequential
 *         number. For example, the first job in 2014 has a job number of
 *         140001.
 * 
 *         * A code representing the employee assigned to the job. Assume that
 *         the code will always be 1, 2, or 3. l A name based on the babysitter
 *         code. Georgette has three babysitters: (1) Cindy, (2) Greg, and (3)
 *         Marcia.
 * 
 * 
 *         * The number of children to be watched. Assume that this number is
 *         always greater than zero.
 * 
 *         * The number of hours in the job. Assume that all hour values are
 *         whole numbers.
 * 
 *         * The fee for the job. Cindy is paid $7 per hour per child. Greg and
 *         Marcia are paid $9 an hour for the first child, and $4 per additional
 *         hour for each additional child. For example, if Greg watches three
 *         children for two hours, he makes $17 per hour for two hours, or $34.
 * 
 * 
 *         Create a constructor for the BabysittingJob class that accepts
 *         arguments for the job number, babysitter code, number of children,
 *         and hours. The constructor determines the babysitter name and fee for
 *         the job. Also include a method that displays every BabysittingJob
 *         object field. Save the file as BabysittingJob.java.
 * 
 *         Next, create an application that prompts the user for data for a
 *         babysitting job. Keep prompting the user for each of the following
 *         values until they are valid:
 * 
 *         * A four-digit year between 2013 and 2025 inclusive * A job number
 *         for the year between 1 and 9999 inclusive * A babysitter code of 1,
 *         2, or 3 * A number of children for the job between 1 and 9 inclusive
 *         * A number of hours between 1 and 12 inclusive
 * 
 *         When all the data entries are valid, construct a job number from the
 *         last two digits of the year and a four-digit sequential number (which
 *         might require leading zeroes). Then, construct a BabysittingJob
 *         object, and display its values. Save the file as
 *         CreateBabysittingJob.java.
 *@date 02-July-2018
 * 
 */
public class BabysittingJob {
	final static String COMPANY_NAME = " Georgette�s Babysitting";
	private int jobNumber;
	private int code;
	private int numberOfChildren;
	private int numberOfHours;
	private int fee;
	private String employee;

	public BabysittingJob(int year, int jobNumber, int code,
			int numberOfChildren, int numberOfHours) {
		super();
		this.jobNumber = calculateJobNumber(year, jobNumber);
		this.code = code;
		this.numberOfChildren = numberOfChildren;
		this.numberOfHours = numberOfHours;
		this.employee = (code == 1 ? "Cindy" : (code == 2 ? "Greg": "Marcia"));
		this.fee = calculateFees();

	}
	/**
	 * 
	 * @param year
	 * @param n
	 * @return
	 */
	private int calculateJobNumber(int year, int n) {
		return Integer.valueOf(String.valueOf(year % 100)
				+ String.format("%04d", n));

	}
	/**
	 * 
	 * @return
	 */
	private int calculateFees() {
		if (this.code == 1) {
			return this.numberOfHours * this.numberOfChildren * 7;
		} else {
			return (this.numberOfHours * 9)
					+ (this.numberOfChildren > 1 ? ((this.numberOfChildren - 1) * 4 * this.numberOfHours)
							: 0);
		}
	}

	public void displayObject() {
		System.out.println("BabysittingJob [jobNumber=" + jobNumber + ", code="
				+ code + ", employeeName=" + employee
				+ ", numberOfChildren=" + numberOfChildren + ", numberOfHours="
				+ numberOfHours + ", fee=$" + fee + "]");
	}
}
