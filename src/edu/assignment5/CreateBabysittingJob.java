package edu.assignment5;

import java.util.Scanner;

public class CreateBabysittingJob {
	public static void main(String[] args) {
		
		Scanner s  = new Scanner(System.in);
		
		int year = userInput(s,"Enter the number of years:","YEAR");
		int jobNumber = userInput(s,"Enter the jobNumber:","JOB_NUMBER");
		int babySitterCode = userInput(s,"Enter the Baby Sitter Code:","BABY_SITTER_CODE");
		int numberOfChildren = userInput(s,"Enter the Number Of Children:","NO_OF_CHILDREN");
		int numberOfHours = userInput(s,"Enter the Number Of Hours:","NO_OF_HOURS");
	
		
		BabysittingJob b = new BabysittingJob(year,jobNumber, babySitterCode, numberOfChildren, numberOfHours);
		b.displayObject();
		
	}

	private static int userInput(Scanner s, String request, String variableString) {
		int variable;
		do {
			System.out.print(request);	
			variable = s.nextInt();
		} while (checkCondition(variable,variableString));
		return variable;
	}
	
	
	private static boolean checkCondition(int variable,String variableString) {
		if("YEAR".equals(variableString)){
			return checkYearCondition(variable);
		}else if("NO_OF_HOURS".equals(variableString)){
			return checkNumberOfHoursCondition(variable);
		}else if("NO_OF_CHILDREN".equals(variableString)){
			return checkNumberOfChildrenCondition(variable);
		}else if("BABY_SITTER_CODE".equals(variableString)){
			return checkBabySitterCodeCondition(variable);
		}else if("JOB_NUMBER".equals(variableString)){
			return checkJobNumberCondition(variable);
		}
		return false;
	}
	
	
	
	private static boolean checkNumberOfHoursCondition(int numberOfHours) {
		return numberOfHours<1 || numberOfHours>12;
	}

	private static boolean checkNumberOfChildrenCondition(int numberOfChildren) {
		return numberOfChildren<1 || numberOfChildren>9;
	}

	private static boolean checkBabySitterCodeCondition(int babySitterCode) {
		return babySitterCode!=1 && babySitterCode!=2 && babySitterCode!=3;
	}

	private static boolean checkJobNumberCondition(int jobNumber) {
		return jobNumber<1 || jobNumber>9999;
	}

	private static boolean checkYearCondition(int year) {
		return  year<2013 || year>2025;
	}
	
	
	
}
