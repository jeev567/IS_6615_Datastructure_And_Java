package edu.assignment5;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         13. Friendly Hal�s Used Cars has four salespeople named Pam, Leo,
 *         Kim, and Bob. Accept values for the number of cars sold by each
 *         salesperson in a month, and create a bar chart that illustrates
 *         sales, similar to the one in Figure 6-33. Save the file as
 *         BarGraph.java.
 * 
 *         Diagram 344 C:\Users\jeev5\Documents\U\Summer 2018\Datastructure and
 *         Java\Resource
 * @date: 02-june-2018
 * 
 */
public class BarGraph {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Number of Car sold by Pam: ");
		int p = s.nextInt();
		System.out.print("Number of Car sold by Leo: ");
		int l = s.nextInt();
		System.out.print("Number of Car sold by Kim: ");
		int k = s.nextInt();
		System.out.print("Number of Car sold by Bob: ");
		int b = s.nextInt();

		displayGraph("Pam", p);
		displayGraph("Leo", l);
		displayGraph("Kim", k);
		displayGraph("Bob", b);

		s.close();

	}

	private static void displayGraph(String name, int carSold) {
		System.out.print(name + "  ");
		for (int i = 0; i < carSold; i++) {
			System.out.print("X");
		}
		System.out.println();
	}
}
