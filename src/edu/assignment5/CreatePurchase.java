package edu.assignment5;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         14. a. Create a class named Purchase. Each Purchase contains an
 *         invoice number, amount of sale, and amount of sales tax. Include set
 *         methods for the invoice number and sale amount. Within the set()
 *         method for the sale amount, calculate the sales tax as 5% of the sale
 *         amount. Also include a display method that displays a purchase�s
 *         details. Save the file as Purchase.java.
 * 
 *         b. Create an application that declares a Purchase object and prompts
 *         the user for purchase details. When you prompt for an invoice number,
 *         do not let the user proceed until a number between 1,000 and 8,000
 *         has been entered. When you prompt for a sale amount, do not proceed
 *         until the user has entered a nonnegative value. After a valid
 *         Purchase object has been created, display the object�s invoice
 *         number, sale amount, and sales tax. Save the file as
 *         CreatePurchase.java.
 * @date 02-July-2018
 */
public class CreatePurchase {
	
	public static void main(String[] args) {
		Scanner s  = new Scanner(System.in);
		
		int invoiceNumber;
		do {
			System.out.print("Enter the invoice number: ");
			invoiceNumber = s.nextInt();
		} while (invoiceNumber<=1000 || invoiceNumber>=8000);
		
		
		double amount;
		do {
			System.out.print("Enter the SalesAmount: ");
			amount = s.nextDouble();
		} while (amount<0);
		
		Purchase p = new Purchase();
		p.setAmountOfSale(amount);
		p.setInvoiceNumber(invoiceNumber);
		
		p.display();/*Calling DIsplay Method to see other calculated values as well*/
		s.close();
		
	}
	
	
}
