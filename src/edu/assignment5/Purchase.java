package edu.assignment5;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         14. a. Create a class named Purchase. Each Purchase contains an
 *         invoice number, amount of sale, and amount of sales tax. Include set
 *         methods for the invoice number and sale amount. Within the set()
 *         method for the sale amount, calculate the sales tax as 5% of the sale
 *         amount. Also include a display method that displays a purchase�s
 *         details. Save the file as Purchase.java.
 * 
 *         b. Create an application that declares a Purchase object and prompts
 *         the user for purchase details. When you prompt for an invoice number,
 *         do not let the user proceed until a number between 1,000 and 8,000
 *         has been entered. When you prompt for a sale amount, do not proceed
 *         until the user has entered a nonnegative value. After a valid
 *         Purchase object has been created, display the object�s invoice
 *         number, sale amount, and sales tax. Save the file as
 *         CreatePurchase.java.
 *@date 02-July-2018 
 */
public class Purchase {
	private int invoiceNumber;
	private double amountOfSale;
	private double amountOfSaleTax;
	private final double ROI = 0.05;
	
	public void setInvoiceNumber(int invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public void setAmountOfSale(double amountOfSale) {
		this.amountOfSale = amountOfSale;
		this.amountOfSaleTax = ROI * amountOfSale;
	}
	
	
	public void display(){
		System.out.println("**************Your Invoice**************");
		System.out.println("Invoice Number: "+this.invoiceNumber);
		System.out.println("Amount of Sale: "+ this.amountOfSale);
		System.out.println("Sale Tax: "+ this.amountOfSaleTax);
		System.out.println("Total: "+ (this.amountOfSale+this.amountOfSaleTax));
		
	}
	

}
