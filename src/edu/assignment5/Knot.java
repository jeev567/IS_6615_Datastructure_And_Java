package edu.assignment5;

/**
 * 
 * @author Jeevan Abraham 
 * 		   
 * 		   5. A knot is a unit of speed equal to one nautical
 *         mile per hour. Write an application that displays every integer knot
 *         value from 15 through 30 and its kilometers per hour and miles per
 *         hour equivalents. One nautical mile is 1.852 kilometers or 1.151
 *         miles. Save the file as Knots.java.
 * @date 02-July-2018
 */
public class Knot {
	
	private static final double _TO_KM = 1.852;
	private static final double _TO_MILES = 1.151;
	
	public static void main(String[] args) {
	
		for (int i = 15; i <= 30; i++) {
			System.out.println(i +" knot = " + (i*_TO_KM)+" km/h");
			System.out.println(i +" knot = " + (i*_TO_MILES)+" miles/h");
			System.out.println("*********************");
		}
		
	}

}
