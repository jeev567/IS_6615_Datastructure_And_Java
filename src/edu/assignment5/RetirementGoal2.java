package edu.assignment5;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 * 
 *         10.
 * 
 *         a. Write an application that prompts a user for the number of years
 *         the user has until retirement and the amount of money the user can
 *         save annually. If the user enters 0 or a negative number for either
 *         value, reprompt the user until valid entries are made. Assume that no
 *         interest is earned on the money. Display the amount of money the user
 *         will have at retirement. Save the file as RetirementGoal.java.
 * 
 *         b. Modify the RetirementGoal application to display the amount of
 *         money the user will have if the user earns 5% interest on the balance
 *         every year. Save the file as RetirementGoal2.java
 * @date 02-July-2018
 */

public class RetirementGoal2 {
	private static final double ROI = 0.05;
	public static void main(String[] args) {
		Scanner s  = new Scanner(System.in);
		
		
		double years =  0;
		double canSaveMoney = 0;
		do {
		System.out.print("Enter the number of years until retirement: ");
		years =  s.nextDouble();
		} while (years<=0);
		
		do {
			System.out.print("Enter the amount you can save: ");
			canSaveMoney =  s.nextDouble();
			} while (canSaveMoney<=0);
			
		
		System.out.print("The total amount you can save is "+  (canSaveMoney * Math.pow(((1+(ROI))),years)));
		s.close();
		
	}
}
