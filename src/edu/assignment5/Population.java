package edu.assignment5;

/**
 * 
 * @author Jeevan Abraham
 * 
 * 
 *         12. Assume that the population of Mexico is 114 million and that the
 *         population increases 1.01 percent annually. Assume that the
 *         population of the United States is 312 million and that the
 *         population is reduced 0.15 percent annually. Write an application
 *         that displays the populations for the two countries every year until
 *         the population of Mexico exceeds that of the United States, and
 *         display the number of years it took. Save the file as
 *         Population.java.
 *@date 02-July-2018
 */
public class Population {
	private static double POPULATION_MEXICO = 114;
	private static double POPULATION_INCREASE_MEXICO = 0.0101;
	private static double POPULATION_USA = 312;
	private static double POPULATION_REDUCE_USA = 0.0015;

	public static void main(String[] args) {
		int years = 0;
		while (POPULATION_MEXICO <= POPULATION_USA) {
			years++;
			POPULATION_MEXICO = POPULATION_MEXICO + (POPULATION_INCREASE_MEXICO * POPULATION_MEXICO);
			POPULATION_USA = POPULATION_USA - (POPULATION_REDUCE_USA * POPULATION_USA);
			System.out.println("Mexico: "+ POPULATION_MEXICO);
			System.out.println("USA: "+POPULATION_USA );
			System.out.println("*********************");

		}
		
		System.out.println("The number of years it took: "+years);
	}

}
