package edu.inclass;

/**
 * 
 * @author Jeevan Abraham
 * @date: 12-June-2018	
 */
public class Circle {
	private int radius;

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
}
