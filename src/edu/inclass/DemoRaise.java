package edu.inclass;

/**
 * 
 * @author Jeevan Abraham
 * @date 05-June-2018
 */
public class DemoRaise {
	private static final double RAISE_RATE = 1.10;

	public static void main(String[] args) {
		double salary = 200.00;
		double startingWage = 800.00;
		System.out.println("Demonstraring some raises");
		predictRaise(400);
		predictRaise(200);
		predictRaise(salary);
		predictRaise(startingWage);
	}

	private static void predictRaise(double salary) {

		System.out.println("Current Salary:" + salary + " After raise:"
				+ salary * RAISE_RATE);

	}

}
