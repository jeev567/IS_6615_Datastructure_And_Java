package edu.inclass;
/**
 * 
 * 
 * @author Jeevan Abraham
 * @date June-20-2018
 * The exercise is to understand the significance of getters and setters
 */
public class Student {
	/**
	 * Variable declaration 
	 */
	private String first_name;
	private String last_name;
	private String sem;
	private String subject;
	String uni_name = "University Of Utah";
	
	
	/**
	 * 
	 * @param first_name
	 * @param last_name
	 * @param sem
	 * @param subject
	 */
	public void setStudent_name(String first_name, String last_name,
			String sem, String subject) {

		this.first_name = first_name;
		this.last_name = last_name;
		this.sem = sem;
		this.subject = subject;
	}
	/**
	 * 
	 * @param first_name
	 * @param last_name
	 */
	public void setStudent_name(String first_name, String last_name) {

		this.first_name = first_name;
		this.last_name = last_name;
	}

	public String getStudent_name() {
		return this.first_name;
	}

	

}
