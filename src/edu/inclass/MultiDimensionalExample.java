package edu.inclass;

/**
 * @date 10-July-2018
 */
public class MultiDimensionalExample {
	public static void main(String[] args) {
		int[][] aryNumbers = new int[6][5];

		aryNumbers[0][0] = 10;
		aryNumbers[0][1] = 10;
		aryNumbers[0][2] = 10;
		aryNumbers[0][3] = 10;
		aryNumbers[0][4] = 10;

		aryNumbers[1][0] = 20;
		aryNumbers[1][1] = 45;
		aryNumbers[1][2] = 56;
		aryNumbers[1][3] = 1;
		aryNumbers[1][4] = 33;

		aryNumbers[2][0] = 30;
		aryNumbers[2][1] = 67;
		aryNumbers[2][2] = 32;
		aryNumbers[2][3] = 14;
		aryNumbers[2][4] = 44;

		int rows = 6;
		int columns = 5;

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.printf("%5d ", aryNumbers[i][j]);
			}
			System.out.println();
		}
	}
}
