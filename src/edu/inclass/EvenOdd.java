package edu.inclass;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * In Class 2
 * 
 * @author jeev567 Write an application that asks a user to enter an integer.
 *         Display a statement that indicates whether the integer is even or
 *         odd. Save the file as EvenOdd.java.
 * @date 29-May-2018
 */

public class EvenOdd {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter a number: ");
		try {
			int input = s.nextInt();
			System.out.println(EvenOrOdd(input));
		} catch (InputMismatchException e) {
			System.out.println("Invalid Entry... Enter an integer value...");
			System.out.println("Program Exiting...");
		}

		s.close();
	}

	private static String EvenOrOdd(int input) {
		return (input % 2 == 0) ? "The number " + input + " is an even number"
				: "The number " + input + " is an odd number";

	}
}
