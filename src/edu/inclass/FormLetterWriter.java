package edu.inclass;

/**
 * 
 * @author Jeevan Abraham
 * @date: 12-June-2018
 */
public class FormLetterWriter {
	public static void main(String[] args) {
		displaySlutation("Kelly");
		displayLetter();
		displaySalutation("Christy", "Johnson");
		displayLetter();

	}

	private static void displaySalutation(String firstName, String lastName) {
		System.out.println("Dear " + firstName + " " + lastName + ",");

	}

	private static void displayLetter() {
		System.out.println("Thankyou for your recent order.");

	}

	private static void displaySlutation(String lastName) {
		System.out.println("Dear Mr. or Mrs. " + lastName + ",");

	}
}
