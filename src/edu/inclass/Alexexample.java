package edu.inclass;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Jeevan Abraham
 * @date 03-July-2018
 */
public class Alexexample {
	public static void main(String[] args) {
		List<String> obj = new ArrayList<>();
		obj.add("Alex");
		obj.add("Thomas");
		obj.add("Susan");
		obj.add("Peter");
		
		/*Displaying arraylist element..*/
		System.out.println("Current elements: "+obj);
		
		/*Adding Object*/
		obj.add("Harry");
		System.out.println("After adding elements: "+obj);
		/*Removing by Object*/
		obj.remove("Alex");
		System.out.println("After removing elements: "+obj);
		/*Removing by index*/
		obj.remove(0);
		System.out.println("After removing by index: "+obj);
		
		
	}
}
