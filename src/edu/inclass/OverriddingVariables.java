package edu.inclass;

/**
 * 
 * @author Jeevan Abraham
 * @date 12-June-2018
 */
public class OverriddingVariables {
	public static void main(String[] args) {

		int aNumber = 10;
		System.out.println("In main(), aNumber is " + aNumber);
		firstMethod();
		System.out.println("Back in main(), aNumber is " + aNumber);
		secondMethod();
		System.out.println("Back in main() again, aNumber is " + aNumber);
	}

	public static void firstMethod() {
		int aNumber = 77;
		System.out.println("In firstMethod(), aNumber is " + aNumber);

	}

	public static void secondMethod() {
		int aNumber = 77;
		System.out.println("In secondMethod(), aNumber is " + aNumber);
		aNumber = 862;
		System.out.println("In secondMethod(),after an assignment aNumber is "
				+ aNumber);
	}

}
