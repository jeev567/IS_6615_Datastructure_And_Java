package edu.inclass;

import java.util.Scanner;

public class PaintCalculator {
	private static final double PRICE_GALLON = 32;
	private static final double SOFT_PER_GAL = 350;

	public static void main(String[] args) {
		double length, width, height, price;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the room's length: ");
		length = s.nextDouble();
		System.out.print("Enter the room's width: ");
		width = s.nextDouble();

		System.out.print("Enter the room's height: ");
		height = s.nextDouble();

		System.out.println("The price to paint the room is $"
				+ computeArea(length, width, height));
		s.close();
	}

	private static double computeArea(double length, double width, double height) {
		double gallon = computeGallons(((length * height * 2) + (width + height * 2)));
		System.out.println("**************Details***************");
		System.out.println("You will need " + gallon + " gallons");
		return gallon * PRICE_GALLON;
	}

	private static double computeGallons(double area) {

		return area / SOFT_PER_GAL;
	}
}
