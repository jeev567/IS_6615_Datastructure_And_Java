package edu.inclass;

import java.util.Scanner;

/**
 * 
 * @author jeev567
 * @date 29-May-2018
 * @4.@a. The Williamsburg Women’s Club offers scholarships to local high school
 *        students who meet any of several criteria. Write an application that
 *        prompts the user for a student’s numeric high school grade point
 *        average (for example, 3.2), the student’s number of extracurricular
 *        activities, and the student’s number of service activities. Display
 *        the message “Scholarship candidate” if the student has any of the
 *        following:
 * 
 * @A grade point average of 3.8 or above and at least one extracurricular
 *    activity and one service activity
 * 
 * @A grade point average below 3.8 but at least 3.4 and a total of at least
 *    three extracurricular and service activities
 * @A grade point average below
 * 
 *    3.4 but at least 3.0 and at least two extracurricular activities and three
 *    service activities
 **/
public class InClass2_2 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the High School Grade Point: ");
		double gradePoint = s.nextDouble();
		System.out
				.println("Enter tge High Student’s number of extracurricular activities:  ");
		double eaNumber = s.nextDouble();
		System.out
				.println("Enter tge High Student’s number of service activities: ");
		double sNumber = s.nextDouble();

		if (checkCondition(gradePoint, eaNumber, sNumber)) {
			System.out.println("Scholarship candidate");
		}
		s.close();
	}

	private static boolean checkCondition(double gradePoint, double eaNumber,double sNumber) {
		return (gradePoint >= 3.8 && (eaNumber >= 1 && sNumber == 1))|| (gradePoint < 3.8 && gradePoint >= 3.4 && (eaNumber + sNumber >= 3)) || (gradePoint >= 3.0 && gradePoint < 3.4 && (eaNumber >= 2 && sNumber == 3));
	}
}
