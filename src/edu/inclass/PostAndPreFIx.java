package edu.inclass;


/**
 * 
 * @author Jeevan Abraham
 * @date 03-July-2018
 */
public class PostAndPreFIx {

	public static void main(String[] args) {
		
		int  n = 5;
		int m = 7;
		
		System.out.println(n++);
		System.out.println(n);
		System.out.println(++m);
		System.out.println(m);
		System.out.println(m++);
		System.out.println(m);
		
	}

}
