package edu.inclass;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 */
public class DemoHorse11 {
	public static void main(String[] args) {
		Horse horse1 = new Horse();
		RaceHorse horse2 = new RaceHorse();
		horse1.setName("Old Paint");
		horse1.setcolor("brown");
		horse1.setBirthYear(2009);
		horse2.setName("Champion");
		horse2.setcolor("black");
		horse2.setBirthYear(2011);
		horse2.setRaces(4);
		System.out.println(horse1.getName() + " is " + horse1.getColor()
				+ " and was born in " + horse1.getBirthYear() + " . ");
		System.out.println(horse2.getName() + " is " + horse2.getColor()
				+ " and was born in " + horse2.getBirthYear() + " . ");
		System.out.println(horse2.getName() + " has been in "
				+ horse2.getRaces() + "races.");
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */

/**
 * 
 */
class Horse {
	private String name;
	private String color;
	private int birthYear;

	public String getName() {
		return name;
	}

	public String getColor() {
		return color;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setName(String n) {
		name = n;

	}

	public void setcolor(String c) {
		color = c;
	}

	public void setBirthYear(int y) {
		birthYear = y;

	}
}

/**
 * 
 */
class RaceHorse extends Horse {
	private int races;

	public int getRaces() {
		return races;
	}

	public void setRaces(int r) {
		races = r;
	}
}
