package edu.inclass;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * @date 26-June-2018
 */
public class BankBalance {
	public static void main(String[] args) {
		double balance;
		int response;
		int year = 1;
		final double INT_RATE = 0.03;
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter initial bank balance: ");
		balance = keyboard.nextDouble();
		System.out.println("Do you want to see next years balance?");
		System.out.println("Enter 1 for yes or any number for no:");
		response = keyboard.nextInt();
		while (response == 1) {
			balance += (balance * INT_RATE);
			System.out.println("After year " + year + " at " + INT_RATE
					+ " interest rate. balance is " + balance);
			year = year + 1;
			System.out.println("Do you want to see balance at the end of "
					+ year + "year?");
			System.out.println("Do you want to see next years balance?");
			System.out.println("Enter 1 for yes or any number for no:");

			response = keyboard.nextInt();
		}
		keyboard.close();
	}
}
