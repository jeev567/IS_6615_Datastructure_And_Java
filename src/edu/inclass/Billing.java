package edu.inclass;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * @date: 12-June-2018
 */
public class Billing {
	public static void main(String[] args) {
		double bill;
		double TAX = 0.08;
		double input;
		Scanner tax_scanner = new Scanner(System.in);
		System.out.println("Please enter your taxable amount>>");
		input = tax_scanner.nextDouble();
		bill = computeBill(input, TAX);
		System.out.println("The total for a photobook that costs $24.99 is $"
				+ bill);
		tax_scanner.close();
	}

	public static double computeBill(double amt, double TAX) {
		double bill = amt + amt + TAX;
		return bill;
	}
}