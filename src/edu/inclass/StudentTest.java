package edu.inclass;
/**
 * 
 * @author Jeevan Abraham
 * @date June-20-2018
 * The exercise is to understand the significance of getters and setters
 */
public class StudentTest {
	public static void main(String[] args) {
		/**
		 * Sample Test cases
		 */
		Student s1 = new Student();
		Student s2 = new Student();
		Student s3 = new Student();
		Student s4 = new Student();
		s1.setStudent_name("Jeevan", "Abraham", "Fall 2017", "IS");
		s2.setStudent_name("Super", "Man", "Spring 2017", "JAVA");
		s3.setStudent_name("Bat", "Man", "Summer 2018", "IS");
		s4.setStudent_name("Wonder", "Woman", "Summer 2018", "Java");
		
		
		System.out.println(s1.getStudent_name());
		System.out.println(s2.getStudent_name());
		System.out.println(s3.getStudent_name());
		System.out.println(s4.getStudent_name());
		
		
		System.out.println(s1.uni_name);/*Since this is not private you can access this variable directly*/
		
		

	}

}
