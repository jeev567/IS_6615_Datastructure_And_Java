package edu.inclass;

/**
 * 
 * @author Jeevan Abraham
 * @date: 12-June-2018	
 */
public class CircleTest {
	public static void main(String[] args) {
			Circle a = new Circle();
			Circle b = new Circle();
			
			a.setRadius(12);
			b.setRadius(16);
			System.out.println("Radius: "+a.getRadius());
			System.out.println("Radius: "+b.getRadius());
			
	} 
	
}
