package edu.inclass;

import java.util.Scanner;

/**
 *
 */
public class Enumerators 
{
enum Major(ACC, CHEM, CIS, ENG, HIS, PHYS);

            public static void main(String[] args)
            {
                Major studentMajor;
                String userEntry;
                int position;
                int comparison;
                    Scanner input = new Scanner(System.in);
                System.out.println("The majors we offer are:");
                for (Major m : Major.values())
                {
                    System.out.println(m + " ");
                }
                System.out.print("\n\nEnter your major >> ");
                userEntry = input.nextLine().toUpperCase();
                studentMajor = Major.valueOf(userEntry);
                System.out.println("You Entered " + studentMajor);
                switch(studentMajor)
                {
                    case ACC:
                    case CIS:
                    System.out.println(" Your major is in the business division");
                    break;
                    case CHEM:
                    case PHYS:
                    System.out.println("Your major is in the Science Division");
                    break;
                    case ENG:
                    case HIS:
                    System.out.println("Your major is in the Humanities Division");
                    break;
                }
            }


}
