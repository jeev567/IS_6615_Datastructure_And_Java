package edu.inclass;


/**
 * 
 * @author Jeevan Abraham
 * @date 03-July-2018
 */
public class ForEachExample1 {
	public static void main(String[] args) {
		String[] students = { "Susan", "Harry", "Thomas", "Peter" };
		for (String s : students) {
			System.out.println(s);
		}
		System.out.println("===========");
		for (int i = 0; i < students.length; i++) {
			System.out.println(students[i]+" ");
		}

	}

}
