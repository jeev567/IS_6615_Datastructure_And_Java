/**
 * 
 * @author Jeevan Abraham
 * @date  15-May-2018
 * @Description: Prints two statements
 * 
 */
package edu.inclass;

/**
 * 
 * @author Jeevan Abraham
 * 
 */
public class InClass1 {

	public static void main(String[] args) {
		System.out.println("Hello World");
		System.out.println("My Second Problem");

	}

}
