package edu.inclass;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * @date 03-July-2018
 */
public class CharecterInfo {

	public static void main(String[] args) {
		char aChar = 'C';
		System.out.println("The charecter is " + aChar);
		if (Character.isUpperCase(aChar)) {
			System.out.println(aChar + " is upper case");
		} else {
			System.out.println(aChar + " is not upper case");
		}

		if (Character.isLowerCase(aChar)) {
			System.out.println(aChar + " is lower case");
		} else {
			System.out.println(aChar + " is not lower case");
		}

		aChar = Character.toLowerCase(aChar);
		System.out.println("After toLowerCase(), aChar is " + aChar);

		aChar = Character.toUpperCase(aChar);
		System.out.println("After toUpperCase(), aChar is " + aChar);

		if (Character.isLetterOrDigit(aChar)) {
			System.out.println(aChar + " is a letter or digit");
		} else {
			System.out.println(aChar + " is not a letter or digit");
		}

		if (Character.isWhitespace(aChar)) {
			System.out.println(aChar + " is a white space");
		} else {
			System.out.println(aChar + " is not a white space");
		}

	}

}
