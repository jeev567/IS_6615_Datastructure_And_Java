package edu.inclass;
/**
 * 
 * @author Jeevan Abraham
 * @date 03-July-2018
 */
public class EightInt {
	public static void main(String[] args) {
		
		int[] k = {1,2,3,4,5,6,7,8};
		
		for (int i = 0; i < k.length; i++) {
			System.out.print(k[i]+" ");
		}
		System.out.println();
		for (int i = k.length-1; i>=0; i--) {
			System.out.print(k[i]+" ");
		}
		
		
	}
}
