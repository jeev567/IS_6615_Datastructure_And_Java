package edu.assignment4;

/**
 * 
 * @author Jeevan Abraham 4. a. Create a class named BloodData that includes
 *         fields that hold a blood type (the four blood types are O, A, B, and
 *         AB) and an Rh factor (the factors are + and �). Create a default
 *         constructor that sets the fields to �O� and �+�, and an overloaded
 *         constructor that requires values for both fields. Include get and set
 *         methods for each field. Save this file as BloodData.java. Create an
 *         application named TestBloodData that demonstrates that each method
 *         works correctly. Save the application as TestBloodData.java.
 * 
 *         b.Create a class named Patient that includes an ID number, age, and
 *         BloodData. Provide a default constructor that sets the ID number to
 *         �0�, the age to 0, and the BloodData to �O� and �+�. Create an
 *         overloaded constructor that provides values for each field. Also
 *         provide get methods for each field. Save the file as Patient.java.
 *         Create an application named TestPatient that demonstrates that each
 *         method works correctly, and save it as TestPatient.java.
 * @date 26-June-2018
 */
public class Patient {

	private String IdNumber;
	private int age;
	private BloodData bloodData;

	/*Default Constructor*/
	public Patient() {
		this.IdNumber = "0";
		this.age =0;
		this.bloodData = new BloodData("O","+");
	}
	/*Overloaded Constructor*/
	public Patient(String IdNumber, int age, String bloodType, String rhFactor) {
		this.IdNumber = IdNumber;
		this.age = age;
		this.bloodData =  new BloodData(bloodType,rhFactor);
	
	}
	
	public String getIdNumber() {
		return IdNumber;
	}
	public int getAge() {
		return age;
	}
	public BloodData getBloodData() {
		return bloodData;
	}

	
}
