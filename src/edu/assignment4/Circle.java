package edu.assignment4;

/**
 * 
 * @author Jeevan Abraham
 * 
 * 
 *         6.a. Create a class named Circle with fields named radius, diameter,
 *         and area. Include a constructor that sets the radius to 1 and
 *         calculates the other two values. Also include methods named
 *         setRadius()and getRadius(). The setRadius() method not only sets the
 *         radius but also calculates the other two values. (The diameter of a
 *         circle is twice the radius, and the area of a circle is pi multiplied
 *         by the square of the radius. Use the Math class PI constant for this
 *         calculation.) Save the class as Circle.java.
 * 
 *         b. Create a class named TestCircle whose main() method declares
 *         several Circle objects. Using the setRadius() method, assign one
 *         Circle a small radius value, and assign another a larger radius
 *         value. Do not assign a value to the radius of the third circle;
 *         instead, retain the value assigned at construction. Display all the
 *         values for all the Circle objects. Save the application as
 *         TestCircle.java.
 * @date 26-June-2018
 */
public class Circle {
	private double radius;
	private double diameter;
	private double area;

	public Circle() {
		setValues(1);
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		setValues(radius);
	}

	public double getDiameter() {
		return diameter;
	}

	public double getArea() {
		return area;
	}

	private void setValues(double radius) {
		this.radius = 1;
		this.diameter = this.radius * 2;
		this.area = Math.PI * Math.pow(this.radius, 2);
	}

}
