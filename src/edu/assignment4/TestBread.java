package edu.assignment4;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         5. a. Create a class for the Tip Top Bakery named Bread with data
 *         fields for bread type (such as �rye�) and calories per slice. Include
 *         a constructor that takes parameters for each field, and include get
 *         methods that return the values of the fields. Also include a public
 *         final static String named MOTTO and initialize it to The staff of
 *         life. Write an application named TestBread to instantiate three Bread
 *         objects with different values, and then display all the data,
 *         including the motto, for each object. Save both the Bread.java and
 *         TestBread.java files.
 * 
 *         b. Create a class named SandwichFilling. Include a field for the
 *         filling type (such as �egg salad�) and another for the calories in a
 *         serving. Include a constructor that takes parameters for each field,
 *         and include get methods that return the values of the fields. Write
 *         an application named TestSandwichFilling to instantiate three
 *         SandwichFilling objects with different values, and then display all
 *         the data for each object. Save both the SandwichFilling.java and
 *         TestSandwichFilling.java files.
 * 
 *         c. Create a class named Sandwich. Include a Bread field and a
 *         SandwichFilling field. Include a constructor that takes parameters
 *         for each field needed in the two objects and assigns them to each
 *         object�s constructor. Write an application named TestSandwich to
 *         instantiate three Sandwich objects with different values, and then
 *         display all the data for each object, including the total calories in
 *         a Sandwich, assuming that each Sandwich is made using two slices of
 *         Bread. Save both the Sandwich.java and TestSandwich.java files
 * @date 26-June-2018
 */
public class TestBread {
	
	public static void main(String[] args) {
		Bread b1 = new Bread("Rye",120);
		Bread b2 = new Bread("White",200);
		Bread b3 = new Bread("Wheat",100);
		
		System.out.println("*******Bread 1********");
		System.out.println("Type: "+b1.getBreadType()+"\n Calories Per Slice: "+b1.getCaloriesPerSlice()+"\n Motto: "+b1.MOTTO);
		System.out.println("*******Bread 2********");
		System.out.println("Type: "+b2.getBreadType()+"\n Calories Per Slice: "+b2.getCaloriesPerSlice()+"\n Motto: "+b2.MOTTO);
		System.out.println("*******Bread 3********");
		System.out.println("Type: "+b3.getBreadType()+"\n Calories Per Slice: "+b3.getCaloriesPerSlice()+"\n Motto: "+b3.MOTTO);
			
		}
		
	}
