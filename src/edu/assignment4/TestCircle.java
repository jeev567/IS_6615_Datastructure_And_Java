package edu.assignment4;

/**
 * 
 * @author Jeevan Abraham
 * 
 * 
 *         6.a. Create a class named Circle with fields named radius, diameter,
 *         and area. Include a constructor that sets the radius to 1 and
 *         calculates the other two values. Also include methods named
 *         setRadius()and getRadius(). The setRadius() method not only sets the
 *         radius but also calculates the other two values. (The diameter of a
 *         circle is twice the radius, and the area of a circle is pi multiplied
 *         by the square of the radius. Use the Math class PI constant for this
 *         calculation.) Save the class as Circle.java.
 * 
 *         b. Create a class named TestCircle whose main() method declares
 *         several Circle objects. Using the setRadius() method, assign one
 *         Circle a small radius value, and assign another a larger radius
 *         value. Do not assign a value to the radius of the third circle;
 *         instead, retain the value assigned at construction. Display all the
 *         values for all the Circle objects. Save the application as
 *         TestCircle.java.
 * @date 26-June-2018       
 */
public class TestCircle {
	public static void main(String[] args) {
		Circle c1 = new Circle();
		c1.setRadius(45);
		
		Circle c2 = new Circle();
		c2.setRadius(2);
		
		Circle c3 = new Circle();
		
		System.out.println("***********Circle 1***********");
		System.out.println("Circle 1 \n Radius: "+c1.getRadius()+"\n Diameter: "+c1.getDiameter()+" \n Area: "+c1.getArea());
		System.out.println("***********Circle 2***********");
		System.out.println("Circle 2 \n Radius: "+c2.getRadius()+"\n Diameter: "+c2.getDiameter()+" \n Area: "+c2.getArea());
		System.out.println("***********Circle 3***********");
		System.out.println("Circle 3 \n Radius: "+c3.getRadius()+"\n Diameter: "+c3.getDiameter()+" \n Area: "+c3.getArea());
	}
}
