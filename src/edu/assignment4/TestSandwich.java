package edu.assignment4;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         5. a. Create a class for the Tip Top Bakery named Bread with data
 *         fields for bread type (such as �rye�) and calories per slice. Include
 *         a constructor that takes parameters for each field, and include get
 *         methods that return the values of the fields. Also include a public
 *         final static String named MOTTO and initialize it to The staff of
 *         life. Write an application named TestBread to instantiate three Bread
 *         objects with different values, and then display all the data,
 *         including the motto, for each object. Save both the Bread.java and
 *         TestBread.java files.
 * 
 *         b. Create a class named SandwichFilling. Include a field for the
 *         filling type (such as �egg salad�) and another for the calories in a
 *         serving. Include a constructor that takes parameters for each field,
 *         and include get methods that return the values of the fields. Write
 *         an application named TestSandwichFilling to instantiate three
 *         SandwichFilling objects with different values, and then display all
 *         the data for each object. Save both the SandwichFilling.java and
 *         TestSandwichFilling.java files.
 * 
 *         c. Create a class named Sandwich. Include a Bread field and a
 *         SandwichFilling field. Include a constructor that takes parameters
 *         for each field needed in the two objects and assigns them to each
 *         object�s constructor. Write an application named TestSandwich to
 *         instantiate three Sandwich objects with different values, and then
 *         display all the data for each object, including the total calories in
 *         a Sandwich, assuming that each Sandwich is made using two slices of
 *         Bread. Save both the Sandwich.java and TestSandwich.java files
 * @date 26-June-2018
 */
public class TestSandwich {
	public static void main(String[] args) {
		Sandwich s1 = new Sandwich("Wheat",100,"Egg",120);
		Sandwich s2 = new Sandwich("White",120,"Egg and Meat",190);
		Sandwich s3 = new Sandwich("White",120,"Egg",120.3);
		
		System.out.println("*******Sandwich 1********");
		System.out.println(" Type: "+s1.getBread().getBreadType()+"\n Calories Per Slice: "+s1.getBread().getCaloriesPerSlice()+"\n Sandwich Filling: "+s1.getSandwichFilling().getFillingType()+"\n Calories Per Servings: "+s1.getSandwichFilling().getCaloriesPerServing()+"\n Total Calories: "+ (s1.getBread().getCaloriesPerSlice() + s1.getSandwichFilling().getCaloriesPerServing()));
		System.out.println("*******Sandwich 2********");
		System.out.println(" Type: "+s1.getBread().getBreadType()+"\n Calories Per Slice: "+s1.getBread().getCaloriesPerSlice()+"\n Sandwich Filling: "+s1.getSandwichFilling().getFillingType()+"\n Calories Per Servings: "+s1.getSandwichFilling().getCaloriesPerServing()+"\n Total Calories: "+ (s1.getBread().getCaloriesPerSlice() + s1.getSandwichFilling().getCaloriesPerServing()));
		System.out.println("*******Sandwich 3********");
		System.out.println(" Type: "+s3.getBread().getBreadType()+"\n Calories Per Slice: "+s3.getBread().getCaloriesPerSlice()+"\n Sandwich Filling: "+s3.getSandwichFilling().getFillingType()+"\n Calories Per Servings: "+s3.getSandwichFilling().getCaloriesPerServing()+"\n Total Calories: "+ (s3.getBread().getCaloriesPerSlice() + s3.getSandwichFilling().getCaloriesPerServing()));
	}
}
