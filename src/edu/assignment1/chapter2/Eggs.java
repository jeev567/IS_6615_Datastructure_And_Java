package edu.assignment1.chapter2;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * @chapter 2
 * @exercise 8
 * @Decription Meadowdale Dairy Farm sells organic brown eggs to local
 *             customers. They charge $3.25 for a dozen eggs, or 45 cents for
 *             individual eggs that are not part of a dozen. Write a class that
 *             prompts a user for the number of eggs in the order and then
 *             display the amount owed with a full explanation. For example,
 *             typical output might be, �You ordered 27 eggs. That�s 2 dozen at
 *             $3.25 per dozen and 3 loose eggs at 45.0 cents each for a total
 *             of $7.85.� Save the class as Eggs.java.
 * 
 * @note: I have made this program close to real time application. It full fills
 *        the the requirement of the question but it also considers various test
 *        cases like:
 * @case 1 Quantity entered 0
 * @Case 2 Quantity entered less that 1 dozen
 * @case 3 Quantity entered not in integer but double
 * @case 4 User input error
 * @case 4 description: If user enters wrong input the program allows only three
 *       attempt to enter correct value and if fails to enter a valid value, the
 *       programs informs the users and exits in a fashionable way. There are
 *       various sub-cases under the case 4 as well.
 * 
 * 
 **/
public class Eggs {
	private static final double _PER_DOZEN = 3.25;
	private static final double _PER_INDIVIDUAL_EGG = 0.45;

	public static void main(String[] args) {
		int count = 3;
		boolean flag = false;
		Scanner s = new Scanner(System.in);

		while (count > 0 && !flag) {
			try {
				System.out
						.print("\nEnter the number of eggs you wish to purchase: ");
				int qty = Integer.parseInt(s.next());
				if (qty < 0)
					throw new NumberFormatException();
				flag = true;
				CaluclateInvoice(qty);
			} catch (NumberFormatException e) {
				count = handleError(count);
			}
		}

		s.close();

	}

	private static void CaluclateInvoice(int qty) {
		System.out
				.println("******************Purchase Summary and Invoice*****************");
		if (qty == 0) {
			System.out.println("You have not purchased any eggs");
		} else if (qty < 12) {
			System.out.println("Total Eggs: " + qty + "$");
			System.out.println("Total Number of Lose Eggs:" + qty + "$");
			System.out.println("Total Price of Lose Eggs:" + qty
					* _PER_INDIVIDUAL_EGG + "$");
		} else {
			int loseeggs = qty % 12;
			int dozens = qty / 12;
			System.out.println("Total Eggs: " + qty);
			if (loseeggs > 0) {
				System.out.println("Total Number of Lose Eggs: " + loseeggs
						+ " at price " + loseeggs * _PER_INDIVIDUAL_EGG + "$");
			}
			System.out.println("Total Number Eggs in dozens: " + dozens+ " at price " + dozens * _PER_DOZEN + "$");
			System.out.println("Total Price is "
					+ (loseeggs > 0 ? (dozens * _PER_DOZEN)
							+ (loseeggs * _PER_INDIVIDUAL_EGG)
							: (dozens * _PER_DOZEN)) + "$");
		}
	}
	private static int handleError(int count) {
		System.out
				.println("\nYou have entered an invalid value..Please enter a positive integer value");
		System.out.println("You are left with " + --count
				+ " attempts to enter correct value..");
		if (count == 0) {
		System.out.println("We are sorry!! You have finished your number of attempts... Please try after some time");
		System.out.println("Program exiting...");
			String k = ".";
			for (int i = 0; i < 4; i++) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				k += "..";
				System.out.println(k);
			}
			System.out.println("Goodbye!!");
		}
		return count;}}
