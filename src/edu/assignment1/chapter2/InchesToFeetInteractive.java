package edu.assignment1.chapter2;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * 
 * @author Jeevan Abraham
 * @chapter 2
 * @exercise 6
 * @Secription
 * @a. Write a class that declares a variable named inches that holds a length
 *     in inches, and assign a value. Display the value in feet and inches; for
 *     example, 86 inches becomes 7 feet and 2 inches. Be sure to use a named
 *     constant where appropriate. Save the class as InchesToFeet.java.
 * @b. Write an interactive version of the InchesToFeet class that accepts the
 *     inches value from a user. Save the class as InchesToFeetInteractive.java.
 **/
public class InchesToFeetInteractive {
	private static final double INCHES_TO_FEET = 0.08333;
	private static final NumberFormat formatter = new DecimalFormat("#0.0");

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		try {
			System.out.print("Enter the value of unit considered as inches: ");
			double inches = s.nextDouble();
			String _output = formatter.format(Double.valueOf(inches
					* INCHES_TO_FEET));
			System.out.println("Input " + inches + " becomes "
					+ _output.split("\\.")[0] + " feet and "
					+ _output.split("\\.")[1] + " inches");

		} catch (InputMismatchException e) {
			System.out.println("Invalid value entered..Program Exiting..");
		}
		s.close();
	}
}
