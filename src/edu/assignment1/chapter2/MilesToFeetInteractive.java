package edu.assignment1.chapter2;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * @author Jeevan Abraham
 * @chapter 2
 * @exercise 4
 * @Secription
 * @a. Write a Java class that declares a named constant that holds the number
 *     of feet in a mile: 5,280. Also declare a variable to represent the
 *     distance in miles between your house and your uncle�s house. Assign an
 *     appropriate value to the variable� for example, 8.5. Compute and display
 *     the distance to your uncle�s house in both miles and feet. Display
 *     explanatory text with the values�for example, The distance to my uncle's
 *     house is 8.5 miles or 44880.0 feet. Save the class as MilesToFeet.java.
 * @b. Convert the MilesToFeet class to an interactive application. Instead of
 *     assigning a value to the distance, accept the value from the user as
 *     input. Save the revised class as MilesToFeetInteractive.java.
 */
public class MilesToFeetInteractive {
	private static final int FEET_IN_A_MILE = 5280;
	public static void main(String[] args) {
		System.out.print("Enter the disctance: ");
		Scanner s = new Scanner(System.in);
		try {
			double distance = s.nextDouble();
			System.out.println("The distance to my uncle's house is "
					+ distance + " miles or " + FEET_IN_A_MILE * distance
					+ " feet");
		} catch (InputMismatchException e) {
			System.out.println("You have entered an invalid value");
		}
		s.close();
	}
}
