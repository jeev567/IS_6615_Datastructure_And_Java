package edu.assignment1.chapter2;
/**
 * 
 * @author Jeevan Abraham
 * @chapter 2
 * @exercise 4
 * @Secription
 * @a. Write a Java class that declares a named constant that represents next
 *     year�s anticipated 10 percent increase in sales for each division of a
 *     company. (You can represent 10 percent as 0.10.) Also declare variables
 *     to represent this year�s sales total in dollars for the North and South
 *     divisions. Assign appropriate values to the variables�for example, 4000
 *     and 5500. Compute and display, with explanatory text, next year�s
 *     projected sales for each division. Save the class as ProjectedSales.java.
 * @b. Convert the ProjectedSales class to an interactive application. Instead
 *     of assigning values to the North and South current year sales variables,
 *     accept them from the user as input. Save the revised class as
 *     ProjectedSalesInteractive.java.
 **/
public class ProjectedSales {
	private static final double ANTICITAED_SALE_PERCENTAGE = 0.10;

	public static void main(String[] args) {
		int currentSaleSouth = 4000;
		int currentSaleNorth = 5500;

		System.out.println("Project Sale for south division: "
				+ currentSaleSouth * ANTICITAED_SALE_PERCENTAGE);
		System.out.println("Project Sale for north division: "
				+ currentSaleNorth * ANTICITAED_SALE_PERCENTAGE);
	}
}
