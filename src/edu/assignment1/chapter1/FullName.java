package edu.assignment1.chapter1;

/**
 * 
 * @author Jeevan Abraham
 * @chapter 1
 * @exercise 5
 * @Secription: Write, compile, and test a class that displays your full name on
 *              the screen. Save the class as FullName.java.
 */
public class FullName {

	public static void main(String[] args) {
		System.out.println("Jeevan Abraham");

	}

}
