package edu.assignment1.chapter1;

/**
 * 
 * @author Jeevan Abraham
 * @chapter 1
 * @exercise 6
 * @Secription Write, compile, and test a class that displays your full name,
 *             e-mail address, and phone number on three separate lines on the
 *             screen. Save the class as PersonalInfo.java.
 */
public class PersonalInfo {
	public static void main(String[] args) {
		System.out.println("Full Name: Jeevan Abraham");
		System.out.println("E-mail: u1154473@utah.edu");
		System.out.println("Address: 425 S. 900 E. SLC, UT 84102");
		System.out.println("Phone Number: +1 801 674 4885");
	}
}
