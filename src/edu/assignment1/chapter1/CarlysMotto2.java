package edu.assignment1.chapter1;
/**
 * 
 * @author Jeevan Abraham
 * @chapter 1
 * @case problem 1
 * @decription Carly�s Catering provides meals for parties and special events.
 *             Write a program that displays Carly�s motto, which is �Carly�s
 *             makes the food that makes it a party.� Save the file as
 *             CarlysMotto.java. Create a second program that displays the motto
 *             surrounded by a border composed of asterisks. Save the file as
 *             CarlysMotto2.java.
 * @Output 
 * 	***********************************************
 *	*Carly�s makes the food that makes it a party.*
 *	***********************************************
 */
public class CarlysMotto2 {
	public static void main(String[] args) {
		//Object created for the class
		CarlysMotto2 c = new CarlysMotto2();
		//Re Use of code..
		System.out.println(c.display(new CarlysMotto()));

	}

	public String display(CarlysMotto c) {
		String roof = "*";
		for (int i = 0; i < c.display().length()+1; i++) {
			roof+="*";
		}
		
		return roof+"\n*"+c.display()+"*\n"+roof;
	}
}
