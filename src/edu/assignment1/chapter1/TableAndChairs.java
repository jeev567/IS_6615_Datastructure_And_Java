package edu.assignment1.chapter1;
/**
 * 
 * @author Jeevan Abraham
 * @chapter 1
 * @exercise 7
 * @Secription Write, compile, and test a class that displays the
 * 			   following pattern on the screen:
 * 			x					 x
 * 			x					 x
 * 			x	  xxxxxxxxxx	 x
 * 			xxxxx x		   x xxxxx
 * 			x	x x		   x x	 x
 * 			x	x x		   x x   x
 */			
public class TableAndChairs {
	public static void main(String[] args){
		printPattern("x                    x",true);
		printPattern("x     xxxxxxxxxx     x",false);
		printPattern("xxxxx x        x xxxxx",false);
		printPattern("x   x x        x x   x",true);
		
	}
	private static void printPattern(String p, boolean b) {
		if (b) {
			for (int i = 0; i < 2; i++) {
				System.out.println(p);
			}
		} else {
			System.out.println(p);
		}

	}
}
