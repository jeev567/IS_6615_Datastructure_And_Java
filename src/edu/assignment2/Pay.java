package edu.assignment2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import edu.util.Date;

/**
 * 
 * @author Jeevan Abraham
 * @Question Barnhill Fastener Company runs a small factory. The company employs
 *           workers who are paid one of three hourly rates depending on skill
 *           level:
 * 
 *           Skill Level 	Hourly Pay Rate ($) 
 *           1 				17.00 
 *           2 				20.00 
 *           3 				22.00 
 *           
 *           Each factory worker might work any number of hours per week; any hours
 *           over 40 are paid at one and one-half times the usual rate. In
 *           addition, workers in skill levels 2 and 3 can elect the following
 *           insurance options:
 * @Option Explanation Weekly      			Cost to Employee ($)
 * @1      Medical insurance        		32.50
 * @2      Dental insurance         		20.00
 * @3      Long-term disability insurance 	10.00 
 * 
 *     Also, workers in skill level 3 can
 *    elect to participate in the retirement plan at 3% of their gross pay.
 *    Write an interactive Java payroll application that calculates the net pay
 *    for a factory worker. 
 *    
 *    The program prompts the user for skill level and
 *    hours worked, as well as appropriate insurance and retirement options for
 *    the employee�s skill level category. The application displays: (1) the
 *    hours worked, (2) the hourly pay rate, (3) the regular pay for 40 hours,
 *    (4) the overtime pay, (5) the total of regular and overtime pay, and (6)
 *    the total itemized deductions. If the deductions exceed the gross pay,
 *    display an error message; otherwise, calculate and display (7) the net pay
 *    after all the deductions have been subtracted from the gross. Save the
 *    file as Pay.java.
 * @date 02-June-2018
 */
public class Pay {
	public static void main(String[] args) {
		//Declare all Variable
		double totalReglarPay = 0;
		double overtimePay = 0;
		double hours = 0;
		double itemizedDeduction =0;
		boolean flag = true;
		int counter = 3;
		int skill=0;
		int insurance = 0;
		int retirementPlan=0;
		
		//Create a dictionary
		Map<Integer,Double> skillMap = new HashMap<Integer, Double>();
		skillMap.put(1, 17.00);
		skillMap.put(2, 20.00);
		skillMap.put(3, 22.00);
		
		//Create a dictionary
		Map<Integer,Double> InsuranceMap = new HashMap<Integer, Double>();
		InsuranceMap.put(1, 32.50);
		InsuranceMap.put(2, 20.00);
		InsuranceMap.put(3, 10.00);
		
		
		System.out.println(" Barnhill Fastener Pay Roll System....");
		Scanner s  = new Scanner(System.in);
		
		System.out.println("Enter the skill level: ");
		//User friendly
		while(check(flag, counter)){
			try {
				String k = s.next();
				skill = Integer.valueOf(k);
				if(skill!=1 && skill!=2 && skill!=3) throw new Exception();
				 flag=false;
				 counter=3;
			} catch (Exception e) {
				counter = errorHandle(flag, counter);
			}
		}
		flag=true;
		while (check(flag, counter)) {
			try {
				System.out.println("Enter the number of hour worked: ");
				String k = s.next();
				hours = Double.valueOf(k);
				 flag=false;
				 counter=3;
			} catch (Exception e) {
				counter = errorHandle(flag, counter);
			}
		}
		flag=true;
		if(skill==2||skill==3){
			while (check(flag, counter)) {
			try {
				System.out.println("Opt for insurance options or press 0 tp escape \nOption   Explanation    		  Weekly Cost to Employee($)\n1        Medical insurance     		  32.50\n2        Dental insurance     		  20.00\n3        Long-term disability insurance   10.00 ");
				String k = s.next();
				insurance = Integer.valueOf(k);
				 flag=false;
				 counter=3;
			 } catch (Exception e) {
				counter = errorHandle(flag, counter);
			 }	
				
			}
		}
		flag=true;
		if(skill==3){
			while(check(flag, counter)){
				try {
					System.out.println("Since you are skill 3, you can participate in the retirement plan at 3% of their gross pay or press 1 to accept or press 0 to skip");
					String k = s.next();
					retirementPlan = Integer.valueOf(k);
					if(retirementPlan !=1 && retirementPlan!=0){
						throw new Exception();
							
					}else{
						flag=false;
						counter=3;
					}
					
				} catch (Exception e) {
					counter = errorHandle(flag, counter);
				}
				
			}
		}
		
		
		System.out.println("***************************PAY SLIP*************************");
	
		System.out.println("The number of hour worked: "+hours );
		System.out.println("The hourly rate: "+skillMap.get(skill)+"$");
		totalReglarPay=(hours<40)?hours*(skillMap.get(skill)):40*(skillMap.get(skill));
		System.out.println("The regular pay for 40 hours: "+totalReglarPay+"$");
		overtimePay = (hours>40)? ((hours-40)*(skillMap.get(skill))*1.5):0;
		System.out.println("The overtime pay: "+ overtimePay+"$");
		System.out.println("The total of regular and overtime pay: "+ (totalReglarPay+overtimePay)+"$");
	
		if(skill==2 || skill==3){
			itemizedDeduction = (retirementPlan==1)?(InsuranceMap.get(insurance)+(0.03*(totalReglarPay+overtimePay))):InsuranceMap.get(insurance);
		}
			System.out.println("The total Itemized deduction: "+ ((itemizedDeduction <(overtimePay+totalReglarPay))?itemizedDeduction+"$":"The deductions are greater than the gross pay :)"));
			System.out.println("The net pay: "+ (totalReglarPay+overtimePay-(itemizedDeduction))+"$");
		
		s.close();
	}

	private static int errorHandle(boolean flag, int counter) {
		flag=true;
		System.out.println("Not a valid Entry..Please try again..");
		System.out.println("Number of attempts left: "+ --counter);
		if(counter==0){
			System.out.println("Program Exiting..Please try after some time...");
		System.exit(-1);;
				}
		return counter;
	}

	private static boolean check(boolean flag, int counter) {
		return flag && counter>0;
	}

}
