package edu.assignment2;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * @Question5. Write an application that displays a menu of three items for the
 *             Jivin� Java Coffee Shop as follows:
 * 
 * @# American 1.99
 * @# Espresso 2.50
 * @# Latte 2.15
 * @Prompt the user to choose an item using the number (1, 2, or 3) that
 *         corresponds to the item, or to enter 0 to quit the application. After
 *         the user makes the first selection, if the choice is 0, display a
 *         total bill of $0. Otherwise, display the menu again. The user should
 *         respond to this prompt with another item number to order or 0 to
 *         quit. If the user types 0, display the cost of the single requested
 *         item. If the user types 1, 2, or 3, add the cost of the second item
 *         to the first, and then display the menu a third time. If the user
 *         types 0 to quit, display the total cost of the two items; otherwise,
 *         display the total for all three selections. Save the file as
 *         Coffee.java.
 * @date 02-June-2018
 * 
 */
public class Coffee {
	private static final NumberFormat formatter = new DecimalFormat("#0.0"); 
	public static void main(String[] args) {
		double totalProce = 0;
		
		//Welcome Message 
		System.out.println("Welcome to Jivin's Cofee Shop...:)");
		List<String> orders = new ArrayList<>();
		//Price Puller Map 
		HashMap<String, Double> m = PreparePricePullMap();
		Scanner s = new Scanner(System.in);
		
		takeOrder(orders, s);
		totalProce = calculatePrice(totalProce, orders, m);
		System.out.println("Thankyou for the order..\nYour total is: "+formatter.format(totalProce)+"$");
		s.close();	
	}
	/**
	 * 
	 * @param totalProce
	 * @param orders
	 * @param m
	 * @return
	 */
	private static double calculatePrice(double totalProce,
			List<String> orders, HashMap<String, Double> m) {
		for (String o : orders) {
			try {
				totalProce+=Double.valueOf(m.get(o));
			} catch (Exception e) {
				System.out.println("Invalid Order: "+o);
			}
		}
		return totalProce;
	}
	/**
	 * 
	 * @param orders
	 * @param s
	 */
	private static void takeOrder(List<String> orders, Scanner s) {
		String currentSelection;
		boolean marker = false;
		do{
			System.out.println("Today's Menu\n 1. American 1.99$\n 2. Espresso 2.50$\n 3. Latte    2.15$\n 0. Quit");
			System.out.println("Enter you coffee order/s: ");
			currentSelection = s.next();
			String[] selctions = currentSelection.split("\\,");
			if(currentSelection.equals("0")) break;
			for (int i = 0; i < selctions.length; i++) {
				if(selctions[i].equals("0")){
					marker=true;
				}else{
					orders.add(selctions[i]);	
				}
			}
		}while(!currentSelection.equals("0")&& !marker);
	}
	private static HashMap<String, Double> PreparePricePullMap() {
		HashMap<String,Double> m = new HashMap<>();
			m.put("1",1.99);
			m.put("2",2.50);
			m.put("3",2.15);
		return m;
	}
	

}
