package edu.assignment2;

/**
 * 
 * @author jeev567
 * @Question Create a class for Shutterbug�s Camera Store, which is having a
 *           digital camera sale. The class is named DigitalCamera, and it
 *           contains data fields for a brand, the number of megapixels in the
 *           resolution, and price. Include a constructor that takes arguments
 *           for the brand and megapixels. If the megapixel parameter is greater
 *           than 10, the constructor sets it to 10.
 * 
 * 
 *           The sale price is set based on the resolution; any camera with 6
 *           megapixels or fewer is $99, and all other cameras are $129. Also
 *           include a method that displays DigitalCamera details. Write an
 *           application named TestDigitalCamera in which you instantiate at
 *           least four objects, prompt the user for values for the camera brand
 *           and number of megapixels, and display all the values. Save the
 *           files as DigitalCamera.java and TestDigitalCamera.java.
 * @date 02-June-2018
 */
public class DigitalCamera {

	private String brand;
	private Double numberOfMegapixel;
	private String price;

	public DigitalCamera(String brand, double numberOfMegapixel) {
		super();
		this.brand = brand;
		this.numberOfMegapixel = (numberOfMegapixel>10)?10:numberOfMegapixel;
		this.price = (numberOfMegapixel<=6)?"$99":"$129";
	}
	
	public void diplay(){
		
		System.out.println("Brand of the camera: "+this.brand);
		System.out.println("Number of Mega-Pixel/s: "+this.numberOfMegapixel);
		System.out.println("Price of the Camera:" +this.price);
	}
}
