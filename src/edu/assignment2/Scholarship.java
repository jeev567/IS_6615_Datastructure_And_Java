package edu.assignment2;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 * @Question 4.The Williamsburg Women�s Club offers scholarships to local high
 *           school students who meet any of several criteria. Write an
 *           application that prompts the user for a student�s numeric high
 *           school grade point average (for example, 3.2), the student�s number
 *           of extracurricular activities, and the student�s number of service
 *           activities. Display the message �Scholarship candidate� if the
 *           student has any of the following:
 * @A.1 A grade point average of 3.8 or above and at least one extracurricular
 *      activity and one service activity
 * @A.2 A grade point average below 3.8 but at least 3.4 and a total of at least
 *      three extracurricular and service activities
 * @A.3 A grade point average below 3.4 but at least 3.0 and at least two
 *      extracurricular activities and three service activities
 * 
 *      If the student does not meet any of the qualification criteria, display
 *      �Not a candidate.� Save the file as Scholarship.java.
 * @date 02-June-2018
 */
public class Scholarship {
	public static void main(String[] args) {
		System.out.println("Williamsburg Women�s Club Scholarships Portal");
		Scanner s = new Scanner(System.in);
		System.out.println("Please enter high school grade point avergae: ");
		double gpa = s.nextDouble();
		System.out.println("Please enter number of extracurricular activities you have participated: ");
		int ea = s.nextInt();
		System.out.println("Please ebter number of service activities you have participated: ");
		int sa = s.nextInt();
		
		
		if((gpa>=3.8 && ea>=1 && sa==1)|| (gpa>=3.4 && gpa<3.8 && (ea+sa>=3)) || (gpa>=3.0 && gpa<3.4 && (ea>=2 && sa==3) ) ){
			System.out.println("Scholarship candidate");
		}else{
			System.out.println("Not a candidate");
		}
		
		s.close();
	}

}
