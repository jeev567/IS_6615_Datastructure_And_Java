package edu.assignment2;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * @Question Create a class for Shutterbug�s Camera Store, which is having a
 *           digital camera sale. The class is named DigitalCamera, and it
 *           contains data fields for a brand, the number of megapixels in the
 *           resolution, and price. Include a constructor that takes arguments
 *           for the brand and megapixels. If the megapixel parameter is greater
 *           than 10, the constructor sets it to 10.
 * 
 * 
 *           The sale price is set based on the resolution; any camera with 6
 *           megapixels or fewer is $99, and all other cameras are $129. Also
 *           include a method that displays DigitalCamera details. Write an
 *           application named TestDigitalCamera in which you instantiate at
 *           least four objects, prompt the user for values for the camera brand
 *           and number of megapixels, and display all the values. Save the
 *           files as DigitalCamera.java and TestDigitalCamera.java.
 *@date 02-June-2018       
 */
public class TestDigitalCamera {

	public static void main(String[] args) {
		
		Scanner s= new Scanner(System.in);
		DigitalCamera[] dlist = new DigitalCamera[4];
		for(int i =0;i<4;i++){
			System.out.print("Enter the brand of camera: ");
			String brand = s.next(); 
			System.out.print("Enter the number of megapixel: ");
			Double pixels = s.nextDouble();
			dlist[i]=new  DigitalCamera(brand, pixels);
			
		}
		
		for (DigitalCamera digitalCamera : dlist) {
			digitalCamera.diplay();
			System.out.println("**************");
		}
		s.close();		
		
	}
}
