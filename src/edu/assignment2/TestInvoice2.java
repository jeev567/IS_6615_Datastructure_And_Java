package edu.assignment2;

/**
 * 
 * @author jeev567
 * @a. Create a class named Invoice that holds an invoice number, balance due,
 *     and three fields representing the month, day, and year when the balance
 *     is due. Create a constructor that accepts values for all five data
 *     fields. Within the constructor, assign each argument to the appropriate
 *     field with the following exceptions:
 * @a.1 If an invoice number is less than 1000, force the invoice number to 0.
 * @a.2 If the month field is less than 1 or greater than 12, force the month
 *      field to 0.
 * @a.3 If the day field is less than 1 or greater than 31, force the day field
 *      to 0.
 * @a.4 If the year field is less than 2011 or greater than 2017, force the year
 *      field to 0. In the Invoice class, include a display method that displays
 *      all the fields on an Invoice object. Save the file as Invoice.java.
 * @b. Write an application containing a main() method that declares several
 *     Invoice objects, proving that all the statements in the constructor
 *     operate as specified. Save the file as TestInvoice.java.
 * @c. Modify the constructor in the Invoice class so that the day is not
 *     greater than 31, 30, or 28, depending on the month. For example, if a
 *     user tries to create an invoice for April 31, force it to April 30. Also,
 *     if the month is invalid, and thus forced to 0, also force the day to 0.
 *     Save the modified Invoice class as Invoice2.java. Then modify the
 *     TestInvoice class to create Invoice2 objects. Create enough objects to
 *     test every decision in the constructor. Save this file as
 *     TestInvoice2.java.
 *@date 02-June-2018
 */

public class TestInvoice2 {

	public static void main(String[] args) {
		System.out.println("Testing Invoice class 2......");
		
		//Class Invoice2
		new Invoice2(1001, 23, -1, 22, 2016).display();
		new Invoice2(1001, 24, 2, 45, 2016).display();
		new Invoice2(1001, 24, 2, 10, 2016).display();
		new Invoice2(1001, 24, 3, 34, 2016).display();
		new Invoice2(1001, 24, 3, 02, 2016).display();
		new Invoice2(1001, 24, 4, 56, 2016).display();
		new Invoice2(1001, 24, 4, 10, 2016).display();

	}
}
