package edu.midterm;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham 
 * 
 * Write a program that accepts the radius of a circle
 *         and compute the area of the circle
 * @1. Your program should have at least one method named areaCircle
 * @2. Your program should have appropriate local variable
 * @3. Your program should ask users to enter two numbers
 * @4. Your program should print out the result
 * @5. Your program should have all appropriate comments
 * 
 * Take screen shoot of your code with the result and upload it here
 */
public class AreaCircle {
	public static void main(String[] args) {
		
		Scanner s  = new Scanner(System.in);
		System.out.print("Please enter the radius of the circle: ");
		double radius =  s.nextDouble();
		System.out.println("The area of the circle with radius "+ radius+" is "+ areaCircle(radius));
		
		s.close();
	}

	private static double areaCircle(double radius) {
		final double _PIE = 3.14;
		return _PIE * radius* radius;
	}
}
