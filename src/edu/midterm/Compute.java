package edu.midterm;
import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         Write a program that accepts the heights and width of a rectangle and
 *         compute the perimeter and area.
 * @Your program should have at least two methods named computeArea(),
 *       computePerimeter()
 * @Your program should have appropriate local variables
 * @Your program should ask users to enter the heights and width of a rectangle
 * @Your program should print out the perimeter and area of a rectangle
 * @Your Program should have all appropriate comments
 */
public class Compute {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Please enter the height of the rectangle: ");
		double height = s.nextDouble(); 
		System.out.print("Please enter the width of the rectangle: ");
		double width = s.nextDouble();
		System.out.println("The area of the rectangle is "+ computeArea(height, width)+ " and the permineter of the rectangle is "+ computePerimeter(height, width));
		s.close();
	}

	private static double computePerimeter(double height, double width) {
		return 2 * (height+ width);
		
	}
	private static double computeArea(double height, double width) {
		return  height* width;
	}
}
