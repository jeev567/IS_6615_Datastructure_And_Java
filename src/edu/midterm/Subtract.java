package edu.midterm;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham A. Write a program that accepts two numbers from user,
 *         subtract the numbers and print the result
 * 
 * @Your program should have at least one method in addition to main method,
 *       named subNumbers()
 * @Your program should have appropriate local variables Your program should ask
 *       users to enter two numbers
 * @Your program should print out the result of subtracting the two numbers
 * @Your program should have all appropriate comments
 * 
 * 
 * 
 **/
public class Subtract {
	public static void main(String[] args) {
		 Scanner s = new Scanner(System.in);
		 System.out.print("Enter the First Number: ");
		 double firstNumber = s.nextDouble();
		 System.out.print("Enter the Second Number: ");
		 double secondNumber = s.nextDouble();
		 System.out.println("The differnce of "+firstNumber+ " and "+secondNumber+ " is "+difference(firstNumber,secondNumber));
		 s.close();
	}

	private static double difference(double a, double b) {
		return a-b;
		
	}
}
