package edu.assignment6;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham 3. a. Write an application for Carl�s Carpentry that
 *         shows a user a list of available items: table, desk, dresser, or
 *         entertainment center. Allow the user to enter a string that
 *         corresponds to one of the options, and display the price as $250,
 *         $325, $420, or $600, accordingly. Display an error message if the
 *         user enters an invalid item. Save the file as CarpentryChoice.java.
 * 
 * 
 *         b. It might not be reasonable to expect users to type long entries
 *         such as �entertainment center� accurately. Modify the CarpentryChoice
 *         class so that as long as the user enters the first three characters
 *         of a furniture item, the choice is considered valid. Save the file as
 *         CarpentryChoice2.java.
 */

public class CarpentryChoice {
	public static void main(String[] args) {
		System.out
				.println("******Welcome to Carl�s Carpentry*****\n Available item are as follows");
		String[] items = { "Table", "Desk", "Dresser", "Entertainment Centre" };
		for (int i = 0; i < items.length; i++) {
			System.out.println("   " + items[i]);
		}
		System.out.print("Please enter the item: ");
		Scanner s = new Scanner(System.in);
		String item = s.nextLine();
		new CarpentryChoice().getPrice(item);
		s.close();
	}

	public void getPrice(String item) {

		System.out
				.println((item.equalsIgnoreCase("table") ? "The price of the "
						+ item.toLowerCase() + " is $" + 250
						: (item.equalsIgnoreCase("desk")) ? "The price of the "
								+ item.toLowerCase() + " is $" + 325
								: (item.equalsIgnoreCase("dresser")) ? "The price of the "
										+ item.toLowerCase() + " is $" + 420
										: (item).equalsIgnoreCase("entertainment centre") ? "The price of the "
												+ item.toLowerCase()
												+ " is $"
												+ 600
												: "Invalid Entry"));
	}
}
