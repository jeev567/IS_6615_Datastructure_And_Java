package edu.assignment6;


/**
 * 
 * @author Jeevan Abraham
 * 
 *         4. Create an application containing an array that stores eight
 *         integers. The application should (1) display all the integers, (2)
 *         display all the integers in reverse order, (3) display the sum of the
 *         eight integers, (4) display all values less than 5, (5) display the
 *         lowest value, (6) display the highest value, (7) calculate and
 *         display the average, and (8) display all values that are higher than
 *         the calculated average value. Save the file as NumberListDemo.java.
 */
public class NumberListDemo {
	public static void main(String[] args) {
		int[] intArray = { 1, 2, 3, 4, 5, 6, 7, 8 };

		displayArray(intArray);
		displayInReverse(intArray);
		displaySumOfArray(intArray, true);
		displayValuesLessThan5(intArray);
		displayTheLowest(intArray);
		displayTheHighestValue(intArray);
		displayTheAverage(intArray);
		displayGreaterThanAverage(intArray);

	}

	private static void displayGreaterThanAverage(int[] intArray) {
		StringBuilder s = new StringBuilder();
		int avearge = displaySumOfArray(intArray, false) / intArray.length;
		s.append("The value greater than the average which is " + avearge
				+ ": [");
		for (int i = 0; i < intArray.length; i++) {
			if (avearge < intArray[i]) {
				s.append(intArray[i] + ",");
			}
		}
		char k[] = s.toString().toCharArray();
		k[s.toString().length() - 1] = ']';
		System.out.println(new String(k));
	}

	private static int displayTheAverage(int[] intArray) {
		int avearge = displaySumOfArray(intArray, false) / intArray.length;
		System.out.println("The average is " + avearge);
		return avearge;
	}

	private static void displayTheHighestValue(int[] intArray) {
		int highest = intArray[0];
		for (int i = 0; i < intArray.length; i++) {
			if (highest < intArray[i])
				highest = intArray[i];
		}
		System.out.println("The highest value is " + highest);

	}

	private static void displayTheLowest(int[] intArray) {
		int lowest = intArray[0];
		for (int i = 0; i < intArray.length; i++) {
			if (lowest > intArray[i])
				lowest = intArray[i];
		}
		System.out.print("The lowest value is " + lowest + "\n");
	}

	private static void displayValuesLessThan5(int[] intArray) {
		StringBuilder s = new StringBuilder();
		s.append("The value less than 5: [");
		for (int i = 0; i < intArray.length; i++) {
			if (intArray[i] < 5) {
				s.append(intArray[i] + ",");
			}
		}
		char k[] = s.toString().toCharArray();
		k[s.toString().length() - 1] = ']';
		System.out.print(new String(k) + "\n");
	}

	private static int displaySumOfArray(int[] intArray, boolean v) {
		int sum = 0;
		for (int i = 0; i < intArray.length; i++) {
			sum += intArray[i];
		}
		if (v)
			System.out.print("The sum of array is " + sum + "\n");
		return sum;

	}

	private static void displayInReverse(int[] intArray) {
		System.out.print("Array Reverse Display: [");
		for (int i = intArray.length - 1; i >= 0; i--) {
			System.out
					.print((i != 0) ? intArray[i] + "," : intArray[i] + "]\n");
		}
	}

	private static void displayArray(int[] intArray) {
		System.out.print("Array Display: [");
		for (int i = 0; i < intArray.length; i++) {
			System.out.print((i != intArray.length - 1) ? intArray[i] + ","
					: intArray[i] + "]\n");
		}
	}
}
