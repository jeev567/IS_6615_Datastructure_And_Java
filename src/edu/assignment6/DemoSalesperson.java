package edu.assignment6;

/**
 * 
 * @author Jeevan Abraham 6. a. Create a class named Salesperson. Data fields
 *         for Salesperson include an integer ID number and a double annual
 *         sales amount. Methods include a constructor that requires values for
 *         both data fields, as well as get and set methods for each of the data
 *         fields. Write an application named DemoSalesperson that declares an
 *         array of 10 Salesperson objects. Set each ID number to 9999 and each
 *         sales value to zero. Display the 10 Salesperson objects. Save the
 *         files as Salesperson.java and DemoSalesperson.java.
 * 
 * 
 *         b. Modify the DemoSalesperson application so each Salesperson has a
 *         successive ID number from 111 through 120 and a sales value that
 *         ranges from $25,000 to $70,000, increasing by $5,000 for each
 *         successive Salesperson. Save the file as DemoSalesperson2.java.
 */
public class DemoSalesperson {
	public static void main(String[] args) {
			Salesperson[] salesPersonArray = new Salesperson[10];
			
			for (int i = 0; i < 10; i++) {
				salesPersonArray[i]= new Salesperson(9999,0);
			}
			
			//Display Sales Person
			for (int i = 0; i < salesPersonArray.length; i++) {
				System.out.println(salesPersonArray[i].toString());
			}
			
	}
	
}
