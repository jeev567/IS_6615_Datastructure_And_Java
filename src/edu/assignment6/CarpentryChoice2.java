package edu.assignment6;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham 3. a. Write an application for Carl�s Carpentry that
 *         shows a user a list of available items: table, desk, dresser, or
 *         entertainment center. Allow the user to enter a string that
 *         corresponds to one of the options, and display the price as $250,
 *         $325, $420, or $600, accordingly. Display an error message if the
 *         user enters an invalid item. Save the file as CarpentryChoice.java.
 * 
 * 
 *         b. It might not be reasonable to expect users to type long entries
 *         such as �entertainment center� accurately. Modify the CarpentryChoice
 *         class so that as long as the user enters the first three characters
 *         of a furniture item, the choice is considered valid. Save the file as
 *         CarpentryChoice2.java.
 */

public class CarpentryChoice2 {

	public void getPrice(String item) {

		System.out
				.println((checkItem(item, "table") ? "The price of the Table is $" + 250
						: (checkItem(item, "desk")) ? "The price of the Desk is $" + 325
								: (checkItem(item, "dresser")) ? "The price of the Dresser is $" + 420
										: checkItem(item,
												"entertainment centre") ? "The price of the Entertainment Centre is $" + 600
												: "Invalid Entry"));
	}

	private boolean checkItem(String item, String inventory) {
		if (item.substring(0, 3).equalsIgnoreCase(inventory.substring(0, 3)))
			return true;
		return false;
	}

	public static void main(String[] args) {
		System.out
				.println("******Welcome to Carl�s Carpentry*****\n Available item are as follows");
		String[] items = { "Table", "Desk", "Dresser", "Entertainment Centre" };
		for (int i = 0; i < items.length; i++) {
			System.out.println("   " + items[i]);
		}
		System.out.print("Please enter the item: ");
		Scanner s = new Scanner(System.in);
		String item = s.nextLine();
		new CarpentryChoice2().getPrice(item);
		s.close();
	}

}
