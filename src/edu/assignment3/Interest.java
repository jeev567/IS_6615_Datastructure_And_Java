package edu.assignment3;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         Write an application that calculates and displays the amount of
 *         money a user would have if his or her money could be invested at 5
 *         percent interest for one year. Create a method that prompts the user
 *         for the starting value of the investment and returns it to the
 *         calling program. Call a separate method to do the calculation, and
 *         return the result to be displayed. Save the program as Interest.java.
 * @date: 12-June-2018
 */
public class Interest {
	private static final double _ROI = 0.05;
	private static final int _TIME = 1;
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the invetment amount: ");
		try {
			System.out.println(calculateOutcome(s.nextDouble()));	
		} catch (Exception e) {
			System.out.println("Invalid Input...");
		}
		
		s.close();
	}

	private static String calculateOutcome(double priciple) {
		return "The amount you will recieve after "+_TIME+" year at "+_ROI*100 + "% rate of interest is "+ (((priciple*_ROI*_TIME))+priciple);
	}

}
