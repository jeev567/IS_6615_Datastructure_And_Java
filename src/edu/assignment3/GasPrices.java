package edu.assignment3;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         When gasoline is $100 per barrel, then the consumerís price at the
 *         pump is between $3.50 and $4.00 per gallon. Create a class named
 *         GasPrices. Its main() method holds an integer variable named
 *         pricePerBarrel to which you will assign a value entered by a user at
 *         the keyboard. Create a method to which you pass pricePerBarrel. The
 *         method displays the range of possible prices per gallon. For example,
 *         if gas is $120 per barrel, then the price at the pump should be
 *         between $4.20 and $4.80. Save the application as GasPrices.java.
 *         
 * @date: 12-June-2018
 */
public class GasPrices {
	private static final double _PRICE_PER_BARREL_MIN = 0.0350;
	private static final double _PRICE_PER_BARREL_MAX = 0.0400;

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter price per barrel: ");
		int pricePerBarrel;
		try {
			pricePerBarrel= s.nextInt();
			System.out.println(showRange(pricePerBarrel));
		} catch (Exception e) {
			System.out.println("Invalid Input...");
		}
		
		
		s.close();
	}

	private static String showRange(int pricePerBarrel) {

		return "Possible range: $" + _PRICE_PER_BARREL_MIN * pricePerBarrel
				+ " and $" + _PRICE_PER_BARREL_MAX * pricePerBarrel;
	}
}
