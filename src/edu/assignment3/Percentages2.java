package edu.assignment3;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 * @4.
 * @a. Create an application named Percentages whose main() method holds two
 *     double variables. Assign values to the variables. Pass both variables to
 *     a method named computePercent() that displays the two values and the
 *     value of the first number as a percentage of the second one. For example,
 *     if the numbers are 2.0 and 5.0, the method should display a statement
 *     similar to �2.0 is 40% of 5.0.� Then call themethod a second time,
 *     passing the values in reverse order. Save the application as
 *     Percentages.java.
 * @b. Modify the Percentages class to accept the values of the two doubles from
 *     a user at the keyboard. Save the file as Percentages2.java.
 * @date: 12-June-2018
 */
public class Percentages2 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		try {
			double value1 = s.nextDouble();
			double value2 = s.nextDouble();
			
			System.out.println(computePercent(value1, value2));
			System.out.println(computePercent(value2, value1));	
		} catch (Exception e) {
			System.out.println("Invalid Input....");
		}
		
		s.close();
	}

	private static String computePercent(double value1, double value2) {
		return value1+" is " +String.valueOf((value1/value2) * 100) + "% of " + value2;
	}

}
