package edu.assignment3;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 * 
 * @6. There are 2.54 centimeters in an inch, and there are 3.7854 liters in a
 *     U.S. gallon. Create a class named MetricConversion. Its main() method
 *     accepts an integer value from a user at the keyboard, and in turn passes
 *     the entered value to two methods. One converts the value from inches to
 *     centimeters and the other converts the same value from gallons to liters.
 *     Each method displays the results with appropriate explanation. Save the
 *     application as MetricConversion.java.
 * @date: 12-June-2018
 * 
 */
public class MetricConversion {
	private static final double _CENT_PER_INCH = 2.54;
	private static final double _LITRE_PER_GALLON = 23.7854;

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Enter the number to convert: ");
		try {
			Integer input = s.nextInt();
			System.out.println(convertInchToCenti(input));
			System.out.println(convertGallonToLitlre(input));
		} catch (Exception e) {
			System.out.println("Invalid Input....");
		}
		
	
		s.close();
	}

	private static String convertGallonToLitlre(Integer input) {
		return input + " gallon/s is equal to " + _LITRE_PER_GALLON * input
				+ " litre/s";
	}

	private static String convertInchToCenti(Integer input) {
		return input + " inch/s is equal to " + _CENT_PER_INCH * input
				+ " centimetre/s";
	}

}
