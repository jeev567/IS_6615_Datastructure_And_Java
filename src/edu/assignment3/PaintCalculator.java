package edu.assignment3;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         Assume that a gallon of paint covers about 350 square feet of wall
 *         space. Create an application with a main() method that prompts the
 *         user for the length, width, and height of a rectangular room. Pass
 *         these three values to a method that does the following:
 * 
 *         Calculates the wall area for a room Passes the calculated wall area
 *         to another method that calculates and returns the number of gallons
 *         of paint needed
 * 
 *         Displays the number of gallons needed Computes the price based on a
 *         paint price of $32 per gallon, assuming that the painter can buy any
 *         fraction of a gallon of paint at the same price as a whole gallon l
 *         Returns the price to the main() method The main() method displays the
 *         final price. For example, the cost to paint a 15- by-20-foot room
 *         with 10-foot ceilings is $64. Save the application as
 *         PaintCalculator.java.
 * @date: 12-June-2018
 * 
 */
public class PaintCalculator {
	private static final double PRICE_GALLON = 32;
	private static final double SOFT_PER_GAL = 350;

	public static void main(String[] args) {
		double length, width, height;
		Scanner s = new Scanner(System.in);
		try {

			System.out.print("Enter the room's length: ");
			length = s.nextDouble();
			System.out.print("Enter the room's width: ");
			width = s.nextDouble();

			System.out.print("Enter the room's height: ");
			height = s.nextDouble();

			System.out.println("The price to paint the room is $"
					+ computeArea(length, width, height));

		} catch (Exception e) {
			System.out.println("Invalid Input...");
		}
		s.close();
	}

	private static double computeArea(double length, double width, double height) {
		double gallon = computeGallons(((length * height * 2) + (width + height * 2)));
		System.out.println("**************Details***************");
		System.out.println("You will need " + gallon + " gallons");
		return gallon * PRICE_GALLON;
	}

	private static double computeGallons(double area) {

		return area / SOFT_PER_GAL;
	}
}
