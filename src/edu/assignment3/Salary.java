package edu.assignment3;

import java.util.Scanner;

/**
 * 
 * @author Jeevan Abraham
 * 
 *         9. Write an application that calculates and displays the weekly
 *         salary for an employee. The main() method prompts the user for an
 *         hourly pay rate, regular hours, and overtime hours. Create a separate
 *         method to calculate overtime pay, which is regular hours times the
 *         pay rate plus overtime hours times 1.5 times the pay rate; return the
 *         result to the main() method to be displayed. Save the program as
 *         Salary.java.
 * 
 */
public class Salary {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		try {
			System.out.print("Enter hourly Rate: ");
			double hr = s.nextDouble();
			System.out.print("Enter the regular hour: ");
			double rh = s.nextDouble();
			System.out.print("Enter the overtime hour: ");
			double oh = s.nextDouble();
			System.out.println(calculateSalary(hr, rh, oh));
		} catch (Exception e) {
			System.out.println("Invalid Input...");
		}

		
		s.close();

	}

	private static String calculateSalary(double hr, double rh, double oh) {

		return "****************************************\nThe Salary is " + hr
				* (rh + (oh * 1.5));
	}

}
